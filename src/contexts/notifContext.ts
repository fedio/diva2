import { createContext } from 'react';
import { Notif } from 'requests/notif';

export default createContext<{ notif: Notif[] | null; setNotif:(notif: Notif[] | null) => void }>({
  notif: null,
  setNotif: () => {},
});
