import React from 'react';

import { BrowserRouter } from 'react-router-dom';
import RootContainer from 'containers/RootContainer';
import { ThemeProvider, Theme, createMuiTheme } from '@material-ui/core/styles';

const theme: Theme = createMuiTheme({
  palette: {
    primary: { main: '#ED1E79' },
  },
});

const App = () => (
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <RootContainer />
    </ThemeProvider>
  </BrowserRouter>
);

export default App;
