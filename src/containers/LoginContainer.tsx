import React, { useContext, useState } from 'react';
import localforage from 'localforage';
import { RouteComponentProps, Redirect } from 'react-router-dom';
import { useForm } from 'hooks/useInputs';
import { decodeUri } from 'utils/url';
import userContext from 'contexts/userContext';
import { validateEmail, validatePassword } from 'utils/validation';
import TextField from 'components/inputs/TextInput';
import Checkbox from '@material-ui/core/Checkbox';
import CustomizedSnackbars from 'components/warning';
import './scss/login.scss';
import useApiState from 'hooks/useApiState';
import { useDidUpdate } from 'hooks/useLifeCycle';
import { login } from 'requests/auth';
import { setAuthorizationBearer } from 'requests/http';
import { CircularProgress } from '@material-ui/core';
import { RegisterButton } from '../components/buttons/RegisterButton';
import { LoginButton } from '../components/buttons/LoginButton';

type IVariant = 'success' | 'warning' | 'error' | 'info';

const LoginContainer = ({ location, history }: RouteComponentProps) => {
  const { user, setUser } = useContext(userContext);
  const [loginState, loginCall] = useApiState(login);

  const [formState, formActions] = useForm({
    initialValues: {
      email: '',
      password: '',
    },
    validation: {
      email: validateEmail,
      password: validatePassword,
    },
    required: ['email', 'password'],
  });

  const { values, errors, touched } = formState;
  const { handleChange } = formActions;
  const { from } = decodeUri(location.search);
  const [openModal, setOpenModal] = useState(false);
  const [variantModal, setVariantModal] = useState<IVariant>('success');
  const [textModal, setTextModal] = useState('');
  const [checked, setChecked] = React.useState(false);

  useDidUpdate(() => {
    if (!loginState.fetching && loginState.data) {
      setAuthorizationBearer(loginState.data.token.accessToken);
      localforage.setItem('auth', loginState.data);
      setUser(loginState.data.user);
    } else if (loginState.errorCode) {
      setOpenModal(true);

      setVariantModal('error');
      setTextModal("Nom d'utilisateur et/ou Mot de passe invalide!");
    }
  }, [loginState.fetching]);

  if (user) {
    return <Redirect to={from || '/profile'} />;
  }

  function onSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (formActions.validateForm()) {
      loginCall(formState.values);
    } else {
      formActions.setTouched({ email: true, password: true });
    }
  }
  const handleCloseModal = () => {
    setOpenModal(false);
  };
  const handleChangeCheckBox = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };
  return (
    <div className="flex_1 containers_login_login height_vh_100">
      <div className="login_container_login">
        <div className="login_title_wrapper_login">
          <span className="login_title_login">SE CONNECTER</span>
        </div>
        <div className="loginCard_login">
          <form onSubmit={onSubmit}>
            <div className="inputWrapperLogin">
              <div className="label-input">Adresse e-mail </div>
              <TextField
                name="email"
                onChange={handleChange}
                value={values.email}
                autoComplete="off"
                error={touched.email && errors.email !== ''}
              />
              <p className="errorMessage">{touched.email ? errors.email : ''}</p>
            </div>
            <div className="inputWrapperLogin">
              <div className="label-input">Mot de passe</div>
              <TextField
                name="password"
                type="password"
                value={values.password}
                onChange={handleChange}
                autoComplete="off"
                error={touched.password && errors.password !== ''}
              />
              {touched.password && errors.password && (
                <p className="errorMessage" style={{ marginTop: '25px' }}>
                  {touched.password ? errors.password : ''}
                </p>
              )}
            </div>
            <div className="container_rester_connecter">
              <Checkbox
                checked={checked}
                onChange={handleChangeCheckBox}
                value="primary"
                inputProps={{ 'aria-label': 'primary checkbox' }}
              />
              <span className="rester">Rester connecter</span>
            </div>

            <LoginButton type="submit">
              Se connecter
              {loginState.fetching && <CircularProgress size={16} className="login_loader" />}
            </LoginButton>
            <div className="creation_conpte_container">
              <a className="forgotPswdLogin" href="/register">
                Je souhaite créer un compte
                {' '}
              </a>
            </div>

            <div className="forgetPassword_container">
              <a href="/forgot-password" className="forgotPswdLogin">
                Mot de passe oublié ?
              </a>
            </div>
          </form>
        </div>
        <CustomizedSnackbars
          variant={variantModal}
          open={openModal}
          handleClose={handleCloseModal}
          message={textModal}
        />
      </div>
    </div>
  );
};

export default LoginContainer;
