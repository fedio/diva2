import React, { useContext, useState } from 'react';
import { RouteComponentProps, Redirect } from 'react-router-dom';
import localforage from 'localforage';
import { useForm } from 'hooks/useInputs';
import useApiState from 'hooks/useApiState';
import { useDidUpdate } from 'hooks/useLifeCycle';
import { register } from 'requests/auth';
import CustomizedSnackbars from 'components/warning';
import { decodeUri } from 'utils/url';
import userContext from 'contexts/userContext';
import { validateEmail, validatePassword, isStringEmpty } from 'utils/validation';
import TextField from '@material-ui/core/TextField';
import { setAuthorizationBearer } from 'requests/http';
// import Checkbox from '@material-ui/core/Checkbox';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import RegisterModal from 'components/registerModal';

/* import InputAdornment from '@material-ui/core/InputAdornment';
 */ import { LoginButton } from '../components/buttons/LoginButton';

import './scss/register.scss';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      '& > *': {
        margin: theme.spacing(1),
      },
      '& input': {
        padding: '8.5px 14px',
      },
      '& input + fieldset': {
        borderRadius: 35,
        backgroundColor: 'RGBA(212,212,212,0.2)',
        width: '100%',
        height: 40,
      },
      '& input:valid + fieldset': {
        borderWidth: 2,
        width: '100%',
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
        borderWidth: 2,
        width: '100%',
      },
      '& input:valid:focus + fieldset': {
        backgroundColor: 'transparent',
        padding: '4px !important', // override inline-style
        width: '100%',
      },
      '& input:valid:-internal-autofill-selected + fieldset': {
        backgroundColor: 'transparent',
      },

      '& label': {
        transform: 'translate(14px, 11px) scale(1)',
      },
    },
    forgotWrapper: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    register: {
      marginTop: '4%',
      width: '88.2%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
  }));
type IVariant = 'success' | 'warning' | 'error' | 'info';

const RegisterContainer = ({ location, history }: RouteComponentProps) => {
  const classes = useStyles();

  const { user, setUser } = useContext(userContext);

  const [formState, formActions] = useForm({
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      secondPassword: '',
      structure: '',
    },
    validation: {
      firstName: isStringEmpty,
      lastName: isStringEmpty,
      email: validateEmail,
      password: validatePassword,
      secondPassword: validatePassword,
    },
  });
  const { values, errors, touched } = formState;
  const { handleChange } = formActions;
  const [registerState, registerCall] = useApiState(register);
  const [openModal, setOpenModal] = useState(false);
  const [variantModal, setVariantModal] = React.useState<IVariant>('success');
  const [textModal, setTextModal] = useState('');
  const { from } = decodeUri(location.search);
  const [openDialog, setOpenDialog] = useState(false);
  useDidUpdate(() => {
    if (!registerState.fetching) {
      setOpenModal(true);
      if (!registerState.errors && !registerState.errorCode && registerState.data) {
        setVariantModal('success');
        setTextModal('inscription effectué avec succès');
        setAuthorizationBearer(registerState.data.token.accessToken);
        localforage.setItem('auth', registerState.data);
        setUser(registerState.data.user);
        setOpenDialog(true);
        /*         history.push('/profile');
         */
      } else {
        setVariantModal('error');
        setTextModal("probléme d'enregistrement");
      }
    }
  }, [registerState.fetching]);
  /*  if (user) {
    return <Redirect to={from || '/'} />;
  } */

  function onSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const {
 email, password, firstName, lastName, structure,
} = formState.values;

    if (formActions.validateForm()) {
      if (values.password === values.secondPassword) {
        registerCall({
          email,
          password,
          firstName,
          lastName,
          structure,
        });
      } else {
        setOpenModal(true);

        setVariantModal('error');
        setTextModal('Les mots de passe ne correspondent pas');
      }
    } else {
      formActions.setTouched({ email: true, password: true });
    }
  }
  const handleCloseModal = () => {
    setOpenModal(false);
  };
  const handleCloseDialog = () => {
    setOpenDialog(false);
    history.push('/profile');
  };
  return (
    <div className="flex_column flex_1 containers_login">
      <div className="login_container">
        <div className="login_title_wrapper padding_h_15" onClick={() => history.push('/')}>
          <span className="login_title1">Badgeons</span>
          <span className="login_title2">les territoires</span>
        </div>
        <div className="login_Card">
          <span className='title1'>Bienvenue sur Badgeons les Territoires ! </span>
          <span className='title2'>
            Badgeons les Territoires est une initiative à échelle territoriale pour faciliter l’insertion
            professionnelle grâce aux compétences transversales. Avec les Open Badges, nous participons à améliorer
            l’employabilité des publics plus fragilisés enrendant visibles les compétences informelles.
          </span>
          <div className="login_card_top_bar" />
          <form onSubmit={onSubmit} className={classes.root}>
            {/*  */}
            <div className="inputWrapper">
              <TextField
                id="outlined-basic"
                label="Nom*"
                name="firstName"
                variant="outlined"
                onChange={handleChange}
                value={values.firstName}
                autoComplete="off"
                error={touched.firstName && errors.firstName !== ''}
                /*   InputProps={{
                  startAdornment: <InputAdornment position="start" />,
                }} */
              />
              {touched.firstName && errors.firstName && (
                <p className="errorMessage">{touched.firstName ? errors.firstName : ''}</p>
              )}
            </div>
            <div className="inputWrapper">
              <TextField
                id="outlined-basic"
                label="Prenom*"
                name="lastName"
                variant="outlined"
                onChange={handleChange}
                value={values.lastName}
                autoComplete="off"
                error={touched.lastName && errors.lastName !== ''}

                /*   InputProps={{
                  startAdornment: <InputAdornment position="start" />,
                }} */
              />

              {touched.lastName && errors.lastName && (
                <p className="errorMessage">{touched.lastName ? errors.lastName : ''}</p>
              )}
            </div>
            <div className="inputWrapper">
              <TextField
                id="outlined-basic"
                label="Address email*"
                name="email"
                variant="outlined"
                onChange={handleChange}
                value={values.email}
                style={{ width: '100%' }}
                autoComplete="off"
                error={touched.email && errors.email !== ''}

                /*   InputProps={{
                  startAdornment: <InputAdornment position="start" />,
                }} */
              />
              {touched.email && errors.email && <p className="errorMessage">{touched.email ? errors.email : ''}</p>}
            </div>
            <div className="inputWrapper">
              <TextField
                id="outlined-basic"
                label="Mot de passe*"
                variant="outlined"
                name="password"
                type="password"
                value={values.password}
                onChange={handleChange}
                style={{ width: '100%' }}
                autoComplete="off"
                error={touched.password && errors.password !== ''}

                /*  InputProps={{
                  startAdornment: <InputAdornment position="start" />,
                }} */
              />
              {touched.password && errors.password && (
                <p className="errorMessage" style={{ marginTop: '25px' }}>
                  {touched.password ? errors.password : ''}
                </p>
              )}
            </div>
            <div className="inputWrapper">
              <TextField
                id="outlined-basic"
                label="Confirmation du mot de passe*"
                variant="outlined"
                name="secondPassword"
                type="password"
                value={values.secondPassword}
                onChange={handleChange}
                style={{ width: '100%' }}
                autoComplete="off"
                error={touched.secondPassword && errors.secondPassword !== ''}

                /*  InputProps={{
                  startAdornment: <InputAdornment position="start" />,
                }} */
              />
              {touched.secondPassword && errors.secondPassword && (
                <p className="errorMessage" style={{ marginTop: '25px' }}>
                  {touched.secondPassword ? errors.secondPassword : ''}
                </p>
              )}
            </div>
            <div className="inputWrapper">
              <TextField
                id="outlined-basic"
                label="Structure"
                name="structure"
                variant="outlined"
                onChange={handleChange}
                value={values.structure}
                style={{ width: '100%' }}
                autoComplete="off"
                /*   InputProps={{
                  startAdornment: <InputAdornment position="start" />,
                }} */
              />
            </div>
            <p>{registerState.errorCode}</p>

            <LoginButton type="submit">S’inscrire</LoginButton>
          </form>
        </div>
      </div>
      <CustomizedSnackbars variant={variantModal} open={openModal} handleClose={handleCloseModal} message={textModal} />
      <RegisterModal open={openDialog} handleClose={handleCloseDialog} />
    </div>
  );
};

export default RegisterContainer;
