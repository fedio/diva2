import React, { useContext, useState } from 'react';
import { RouteComponentProps, Redirect } from 'react-router-dom';
import { useForm } from 'hooks/useInputs';
import useApiState from 'hooks/useApiState';
import { useDidUpdate } from 'hooks/useLifeCycle';
import { resePassword } from 'requests/auth';
import CustomizedSnackbars from 'components/warning';
import { decodeUri } from 'utils/url';
import userContext from 'contexts/userContext';
import { validatePassword } from 'utils/validation';
import TextField from '@material-ui/core/TextField';
//import Checkbox from '@material-ui/core/Checkbox';
//import FormControlLabel from '@material-ui/core/FormControlLabel';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { LoginButton } from '../components/buttons/LoginButton';

import './scss/register.scss';
//import { conditionalExpression } from '@babel/types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      '& > *': {
        margin: theme.spacing(1),
      },
      '& input': {
        padding: '8.5px 14px',
      },
      '& input + fieldset': {
        borderRadius: 35,
        backgroundColor: 'RGBA(212,212,212,0.2)',
        width: '100%',
        height: 40,
      },
      '& input:valid + fieldset': {
        borderWidth: 2,
        width: '100%',
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
        borderWidth: 2,
        width: '100%',
      },
      '& input:valid:focus + fieldset': {
        backgroundColor: 'transparent',
        padding: '4px !important', // override inline-style
        width: '100%',
      },
      '& input:valid:-internal-autofill-selected + fieldset': {
        backgroundColor: 'transparent',
      },

      '& label': {
        transform: 'translate(14px, 11px) scale(1)',
      },
    },
    forgotWrapper: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    register: {
      marginTop: '4%',
      width: '88.2%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
  }));
type IVariant = 'success' | 'warning' | 'error' | 'info';

const RegisterContainer = ({ location, history }: RouteComponentProps) => {
  const classes = useStyles();

  const { user } = useContext(userContext);

  const [formState, formActions] = useForm({
    initialValues: {
      password: '',
      confirmPassword: '',
    },
    validation: {
      password: validatePassword,
      confirmPassword: validatePassword,
    },
  });
  const { values, errors, touched } = formState;
  const { handleChange } = formActions;
  const [resetState, resetCall] = useApiState(resePassword);
  const [openModal, setOpenModal] = useState(false);
  const [variantModal, setVariantModal] = React.useState<IVariant>('success');
  const [textModal, setTextModal] = useState('');
  const { from } = decodeUri(location.search);
  useDidUpdate(() => {
    if (!resetState.fetching) {
      setOpenModal(true);
      if (!resetState.errors && !resetState.errorCode) {
        setVariantModal('success');
        setTextModal('mot de passe changé avec succès');
        setInterval(() => {
          history.push('/login');
        }, 3000);
      } else {
        setVariantModal('warning');
        setTextModal('probleme de changement de mot de passe');
      }
    }
  }, [resetState.fetching]);
  if (user) {
    return <Redirect to={from || '/'} />;
  }

  function onSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const { password, confirmPassword } = values;
    if (formActions.validateForm()) {
      const path = location.search;
      if (path.includes('token', 1)) {
        const token = path.slice(path.indexOf('=') + 1, path.length);
        if (token !== '' && password === confirmPassword) {
          resetCall({ token, password });
        }
      }
    } else {
      formActions.setTouched({ password: true, confirmPassword: true });
    }
  }
  const handleCloseModal = () => {
    setOpenModal(false);
  };
  return (
    <div className="flex_column flex_1 containers_login">
      <div className="login_container">
        <div className="login_title_wrapper padding_h_15">
          <span className="login_title1">Badgeons</span>
          <span className="login_title2">les territoires</span>
        </div>
        <div className="login_Card">
          <form onSubmit={onSubmit} className={classes.root}>
            <div className="inputWrapper">
              <TextField
                id="outlined-basic"
                label="Mot de passe"
                variant="outlined"
                name="password"
                type="password"
                value={values.password}
                onChange={handleChange}
                style={{ width: '100%' }}
                autoComplete="off"
              />
              <p className="errorMessage">{touched.password ? errors.password : ''}</p>
            </div>
            <div className="inputWrapper">
              <TextField
                id="outlined-basic"
                label="Mot de passe"
                variant="outlined"
                name="confirmPassword"
                type="password"
                value={values.confirmPassword}
                onChange={handleChange}
                style={{ width: '100%' }}
                autoComplete="off"
              />
              <p className="errorMessage">{touched.confirmPassword ? errors.confirmPassword : ''}</p>
            </div>
            <LoginButton type="submit">confirmer</LoginButton>
          </form>
          <CustomizedSnackbars
            variant={variantModal}
            open={openModal}
            handleClose={handleCloseModal}
            message={textModal}
          />
        </div>
      </div>
    </div>
  );
};

export default RegisterContainer;
