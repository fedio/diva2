import React, { useState, useEffect, useContext } from 'react';
import useApiState from 'hooks/useApiState';
import { getUserBadges, updateBadge } from 'requests/badges';
import { getCandidatureCount } from 'requests/candidature';
import Warning from 'components/warning';
import Modal from '@material-ui/core/Modal/Modal';
import SubmitButton from 'components/buttons/SubmitButton';
import { useDidMount } from 'hooks/useLifeCycle';


import './scss/profile.scss';
import Loader from 'components/ui/Loader';
import { Paper } from '@material-ui/core';
import YNQInput from 'components/inputs/ YNQInput';
import navigationContext from 'contexts/navigationContext';

const ProfileContainer = () => {
  const [badgesState, getBadges] = useApiState(getUserBadges);
  const [updateBadgeState, updateBadgeCall] = useApiState(updateBadge);
  const [candidatureCountState, candidatureCountCall] = useApiState(getCandidatureCount);
  const { setHeaderProps } = useContext(navigationContext);
  const [open, setOpen] = useState(false);
  const [openBadgeIndex, setOpenBadgeIndex] = useState(-1);
  const [isPublic, setIsPublic] = useState(false);
  const [fetching, setFetching] = useState(false);

  const { errorCode, data } = badgesState;

  useDidMount(() => {
    candidatureCountCall({ status: 'pending' });
  });

  useEffect(() => {
    if (candidatureCountState.data) {
      setHeaderProps({ count: candidatureCountState.data.count });
    }
    // eslint-disable-next-line
  }, [candidatureCountState.data]);

  useEffect(() => {
    if (!updateBadgeState.fetching && !updateBadgeState.errorCode) {
      setOpenBadgeIndex(-1);
      getBadges({ perPage: -1 });
      if (!fetching) setFetching(true);
    }
    // eslint-disable-next-line
  }, [updateBadgeState.fetching]);

  useEffect(() => {
    if (badgesState.fetching !== fetching) {
      setFetching(badgesState.fetching);
    }
    // eslint-disable-next-line
  }, [badgesState.fetching]);

  useEffect(() => {
    if (errorCode) {
      setOpen(true);
    }
  }, [errorCode]);

  function handleClose() {
    setOpen(false);
  }

  function onSelect() {
    if (data && data.data[openBadgeIndex]) {
      updateBadgeCall(data.data[openBadgeIndex]._id, { isPublic });
    }
  }

  function closeModal() {
    setOpenBadgeIndex(-1);
  }

  return <div className="profile_container flex_column">welcome profile page</div>;
};
export default ProfileContainer;
