import React, { useContext, useState } from 'react';

import { RouteComponentProps, Redirect } from 'react-router-dom';
import { useForm } from 'hooks/useInputs';
import { useDidUpdate } from 'hooks/useLifeCycle';
import CustomizedSnackbars from 'components/warning';
import { decodeUri } from 'utils/url';
import userContext from 'contexts/userContext';
import { validateEmail } from 'utils/validation';
import TextField from '@material-ui/core/TextField';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { forgotPassword } from 'requests/auth';
import useApiState from 'hooks/useApiState';

/* import InputAdornment from '@material-ui/core/InputAdornment';
 */ import { LoginButton } from '../components/buttons/LoginButton';
import './scss/forgot.scss';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      '& > *': {
        margin: theme.spacing(1),
      },
      '& input': {
        padding: '8.5px 14px',
      },
      '& input + fieldset': {
        borderRadius: 35,
        backgroundColor: 'RGBA(212,212,212,0.2)',
        width: '100%',
        height: 40,
      },
      '& input:valid + fieldset': {
        borderWidth: 2,
        width: '100%',
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
        borderWidth: 2,
        width: '100%',
      },
      '& input:valid:focus + fieldset': {
        backgroundColor: 'transparent',
        padding: '4px !important', // override inline-style
        width: '100%',
      },
      '& label': {
        transform: 'translate(14px, 11px) scale(1)',
      },
    },
    forgotWrapper: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    register: {
      marginTop: '8%',
      width: '88.2%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
  }));
type IVariant = 'success' | 'warning' | 'error' | 'info';

const LoginContainer = ({ location, history }: RouteComponentProps) => {
  const classes = useStyles();

  const { user } = useContext(userContext);
  const [forgotState, forgotCall] = useApiState(forgotPassword);

  const [formState, formActions] = useForm({
    initialValues: {
      email: '',
    },
    validation: {
      email: validateEmail,
    },
  });

  const { values, errors, touched } = formState;
  const { handleChange } = formActions;
  const { from } = decodeUri(location.search);
  const [openModal, setOpenModal] = useState(false);
  const [variantModal, setVariantModal] = React.useState<IVariant>('success');
  const [textModal, setTextModal] = useState('');
  useDidUpdate(() => {
    if (!forgotState.fetching) {
      setOpenModal(true);
      if (!forgotState.errorCode) {
        setVariantModal('success');
        setTextModal(
          `Un email a bien été envoyé à l’adresse ${formState.values.email}. Veuillez suivre la procédure pour réinitialiser votre mot de passe.`,
        );
      } else {
        setVariantModal('warning');
        setTextModal(
          'Cette adresse email n’est pas connue dans notre base de données, veuillez réessayer avec une adresse email valide ou créerun compte.',
        );
      }
    }
  }, [forgotState.fetching]);
  if (user) {
    return <Redirect to={from || '/'} />;
  }

  function onSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (formActions.validateForm()) {
      forgotCall(formState.values);
    } else {
      formActions.setTouched({ email: true });
    }
  }
  const handleCloseModal = () => {
    setOpenModal(false);
  };
  return (
    <div className="flex_column flex_1 containers_forgot  height_vh_100">
      <div className="forgot_container">
        <div onClick={() => history.push('/')} className="forgot_title_wrapper padding_h_15">
          <span className="forgot_title1">Badgeons</span>
          <span className="forgot_title2">les territoires</span>
        </div>
        <div className="forgot_Card margin_v_500" style={{ height: 'auto' }}>
          <form onSubmit={onSubmit} className={classes.root}>
            <div className="title">Récupération de votre mot de passe</div>
            <div>
              Pour débuter la procédure de récupération de votre mot de passe, veuillez renseigner ci-dessous votre
              adresse email et valider le formulaire.
            </div>
            <div className="inputWrapper">
              <TextField
                id="outlined-basic"
                label="Address email"
                name="email"
                variant="outlined"
                onChange={handleChange}
                value={values.email}
                style={{ width: '100%' }}
                /*   InputProps={{
                  startAdornment: <InputAdornment position="start" />,
                }} */
              />
            </div>
            <p className="errorMessage" style={{ color: 'red' }}>
              {touched.email ? errors.email : ''}
            </p>

            <div className={classes.forgotWrapper}>
              <a href="/login" className="forgotPswd">
                Revenir sur la page de connexion
              </a>
            </div>

            <LoginButton type="submit" style={{ width: '57.2%' }}>
              Envoyer
            </LoginButton>
          </form>
          <CustomizedSnackbars
            variant={variantModal}
            open={openModal}
            handleClose={handleCloseModal}
            message={textModal}
          />
        </div>
      </div>
    </div>
  );
};

export default LoginContainer;
