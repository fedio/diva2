import React, { useState } from 'react';
import { Switch, useHistory } from 'react-router-dom';
import localforage from 'localforage';
import UserContext from 'contexts/userContext';
// hoc
import Route from 'layout/Route';

// containers
import HomeContainer from 'containers/HomeContainer';
import LoginContainer from 'containers/LoginContainer';
import ForgotPassword from 'containers/ForgotPassword';
import ProfileContainer from 'containers/ProfileContainer';

// header
import Header from 'layout/Header';
import Footer from 'layout/Footer';
import FooterLogo from 'layout/FooterLogo';

import CustomizedSnackbars from 'components/warning';
// experimental assets
import Register from 'containers/RegisterContainer';
import reset from 'containers/resetPassword';
import AppBar from '@material-ui/core/AppBar';
import Button from 'components/button';
import Typography from '@material-ui/core/Typography';
// styles

// hooks
import { useDidMount, useDidUpdate } from 'hooks/useLifeCycle';
import { refreshToken, User, resendConfirm } from 'requests/auth';
import './scss/root.scss';
import { setAuthorizationBearer } from 'requests/http';
import useApiState from 'hooks/useApiState';

// import ModalActiveUser from 'components/modalActiveUser';

// Modal

type IVariant = 'success' | 'warning' | 'error' | 'info';

async function onStart(): Promise<User | null> {
  try {
    const auth: any = await localforage.getItem('auth');
    if (!auth) throw new Error('no user');
    const { token, user } = auth;
    const nextToken = await refreshToken({ email: user.email, refreshToken: token.refreshToken });

    if (nextToken.status === 'OK') {
      localforage.setItem('auth', { user, token: nextToken.data });
      setAuthorizationBearer(nextToken.data.accessToken);
      return user;
    }

    throw new Error('end');
  } catch (e) {
    return null;
  }
}
const RootContainer = () => {
  const [startup, setStartup] = useState(false);
  const [user, setUser] = useState<User | null>(null);
  const history = useHistory();
  const [resendState, resendCall] = useApiState(resendConfirm);
  const [variantModal, setVariantModal] = React.useState<IVariant>('success');
  const [textModal, setTextModal] = useState('');
  const [openModal, setOpenModal] = useState(false);

  const handleCloseModal = () => {
    setOpenModal(false);
  };
  useDidMount(() => {
    onStart().then((nextUser) => {
      if (nextUser) setUser(nextUser);
      setStartup(true);
    });

    history.listen(() => {
      window.scrollTo({ top: 0, left: 0 });
    });
  });
  const location = history.location.pathname;
  const resend = () => {
    resendCall();
  };
  useDidUpdate(() => {
    if (resendState.fetching) {
      setOpenModal(true);
      if (!resendState.errors) {
        setVariantModal('success');
        setTextModal('email envoyé avec succès');
      } else {
        setVariantModal('error');
        setTextModal("probléme d'envoie");
      }
    }
  });

  if (!startup) return <div />;
  return (
    <UserContext.Provider value={{ user, setUser }}>
      <div className="containers_root">
        {location !== '/register' && user && !user.active && (
          <AppBar position="static" className="appbar_root_confirm">
            <Typography variant="h6">
              votre compte n&apos;est pas activé , merci de verifier votre boite email
            </Typography>
            <Button className="renvoyer_button_root" onClick={resend}>
              Renvoyer
            </Button>
          </AppBar>
        )}
        <CustomizedSnackbars
          variant={variantModal}
          open={openModal}
          handleClose={handleCloseModal}
          message={textModal}
        />
        <Header />
        <Switch>
          <Route path="/" exact component={HomeContainer} />
          <Route path="/register" component={Register} />
          <Route path="/reset" component={reset} />
          <Route exact path="/login" component={LoginContainer} />
          <Route exact path="/forgot-password" component={ForgotPassword} />
          <Route exact path="/profile" component={ProfileContainer} addFooter protected />
          <Route render={() => <h1>404 not found</h1>} />
        </Switch>
        <FooterLogo />
      </div>
    </UserContext.Provider>
  );
};

export default RootContainer;
