import { User } from './auth';
import {
  axiosGet,
  Response,
  axiosPostFilesData,
  axiosPost,
  SuccessResponse,
  ErrorResponse,
  ListResponse,
  axiosPatch,
} from './http';

import { Category } from './categories';

interface Filterbadge {
  search?: string;
  currentPage?: number;
  perPage?: number;
  others?: string;
}

export interface Criteria {
  _id: string;
  title: string;
  type: 'pdf' | 'image' | 'text';
  description: string;
}

export interface Badge {
  _id: string;
  title: string;
  description: string;
  mainCategory: Category;
  secondaryCategory?: Category;
  tags: string[];
  isPublic: boolean;
  resumeCriterion: string;
  startDate: string;
  endDate: string;
  criterion: Criteria[];
  deliveranceAuto: boolean;
  issuerId: string;
  image: string;
  createdAt: string;
  candidaturesCount: number;
  endo: {
    _id: string;
idEndosseur: User;
typeEndo: string;
idBadge: string;
text: string;
status: string;
email: string;
name: string;
createdAt: string;
updatedAt: string;
  }[]
}
export const badges = (params?: Filterbadge): Promise<Response<ListResponse<Badge>>> => axiosGet('/badge', { params });

export const uploadModel = (data: FormData): Promise<Response<string>> => axiosPostFilesData('/badge/upload', { data });

interface CriteriaParams {
  title: string;
  type: string;
  description: string;
}

export const addCriteria = (data: CriteriaParams): Promise<Response<Criteria>> => axiosPost('/critere', { data });

interface AddBadgeData {
  title: string;
  description: string;
  mainCategory: string;
  secondaryCategory?: string;
  isPublic: boolean;
  resumeCriterion: string;
  startDate: string;
  endDate: string;
  deliveranceCode?: string;
  image: string;
  criterion: string[];
  tags: string[];
}

export const addBadge = (data: AddBadgeData): Promise<Response<Badge>> => axiosPost('/badge', { data });

interface CreateBadgeData {
  tags: string[];
  title: string;
  mainCategory: string;
  secondaryCategory?: string;
  description: string;
  public: boolean;
  model: Blob;
  criteriaSummary: string;
  releaseStartDate: string;
  releaseEndDate: string;
  releaseCode?: string;
  expectedCriteria: { title: string; type: string; description: string }[];
}

export const createBadge = async (data: CreateBadgeData): Promise<Response<Badge>> => {
  const form = new FormData();
  form.append('file', new File([data.model], 'filename', { type: data.model.type }));
  const uploadResponse = await uploadModel(form);

  if (uploadResponse.status !== 'OK') {
    return uploadResponse;
  }
  const expectedCriteriaResponse = await Promise.all(data.expectedCriteria.map((criteria) => addCriteria(criteria)));

  const criteriaError = expectedCriteriaResponse.find((criteriaResponse) => criteriaResponse.status !== 'OK');

  if (criteriaError) return criteriaError as ErrorResponse;

  return addBadge({
    title: data.title,
    description: data.description,
    mainCategory: data.mainCategory,
    secondaryCategory: data.secondaryCategory || undefined,
    isPublic: data.public,
    resumeCriterion: data.criteriaSummary,
    startDate: data.releaseStartDate,
    endDate: data.releaseEndDate,
    deliveranceCode: data.releaseCode,
    image: uploadResponse.data,
    criterion: expectedCriteriaResponse.map((response) => (response as SuccessResponse<Criteria>).data._id),
    tags: data.tags,
  });
};

export interface DonatedBadge extends Badge {
  status: string;
}
export interface AwardBadgeParams {
  recepientName: string;
  recepientEmail: string;
  idCandidature: string;
}

export const getBadge = (id: string): Promise<Response<DonatedBadge>> => axiosGet(`/badge/${id}`);

export const getUserBadges = (params?: Filterbadge): Promise<Response<ListResponse<Badge>>> =>
  axiosGet('/badge/user', { params });

export const updateBadge = (id: string, data: any): Promise<any> => axiosPatch(`/badge/${id}`, { data });

export const awardBadge = (data: AwardBadgeParams): Promise<Response<any>> => axiosPost('/awardBadge', { data });
