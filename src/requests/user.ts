import { axiosPatch, Response } from './http';

export const updateUser = (
  data: {
    email: string;
    password?: string;
    oldPassword?: string;
    firstName: string;
    lastName: string;
  },
  id: string,
): Promise<Response<any>> => axiosPatch(`/users/${id}`, { data, sendToken: false });
