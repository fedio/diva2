import {
 axiosGet, Response, axiosPatch, axiosDelete, axiosPost, axiosPostFilesData,
} from './http';
import { Badge } from './badges';

export type Status = 'accepted' | 'refused' | 'pending';

interface PatchCandidatureParams {
  status?: Status;
  id?: string;
  endorsement?: Status;
}

export interface CandidatureBase {
  _id: string;
  criterions: string[];
  status: Status;
  date: string;
  createdAt: string;
  endorsement: Status;
}

export interface Candidature extends CandidatureBase {
  idBadge: string;

  idUser: {
    role: string;
    active: boolean;
    _id: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    activeToken: string;
    createdAt: string;
    updatedAt: string;
  };
}

export interface CandidatureById extends CandidatureBase {
  idBadge: {
    _id: string;
    title: string;
    description: string;
    image: string;
    issuerId: string;
  };
  idUser: {
    role: string;
    active: boolean;
    _id: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    activeToken: string;
    createdAt: string;
    updatedAt: string;
  };
}

export const getCandidature = (): Promise<Response<any>> => axiosGet('/candidature/user');
export const getMyCandidature = (): Promise<Response<any>> => axiosGet('/candidature/myCandidature');

export const getCandidatureById = (id: string): Promise<Response<CandidatureById>> => axiosGet(`/candidature/${id}`);

export const PatchCandidature = ({ id, ...data }: PatchCandidatureParams): Promise<Response<any>> =>
  axiosPatch(`/candidature/${id}`, { data });

export interface BadgeCandidatureParams {
  status?: string;
  endorsed?: boolean;
}

export const getBadgeCandidature = (
  id: string,
  params: BadgeCandidatureParams = {},
): Promise<Response<{ candidatures: Candidature[]; badge: Badge }>> => axiosGet(`/candidature/badge/${id}`, { params });

export const deleteCandidature = (id: string): Promise<any> => axiosDelete(`/candidature/${id}`);

export const submitTextCritere = (data: { title: string; type: string; description: string }): Promise<Response<any>> =>
  axiosPost('/critere', { data, sendToken: true });

export const submitFileCriter = (data: {
  title: string;
  type: string;
  file: string | Blob;
}): Promise<Response<any>> => {
  const dataForm = new FormData();
  dataForm.set('title', data.title);
  dataForm.set('type', data.type);
  dataForm.set('file', data.file);

  return axiosPostFilesData('/critere/upload', { data: dataForm, sendToken: true });
};

export interface AddCandidatureData {
  criterions: string[];
  idBadge: string;
  endorsement: boolean;
  deliveranceCode?: string;
}

export const addCandidature = (data: AddCandidatureData): Promise<Response<any>> =>
  axiosPost('candidature/', { data, sendToken: true });

export const getCandidatureCount = (params: { status?: string } = {}): Promise<Response<{ count: number }>> =>
  axiosGet('candidature/user/count', { params });

/* export const getEndorsement = (id: string): Promise<Response<{ candidatures: Candidature[]; badge: Badge }>> =>
  axiosGet(`/endossement/${id}`); */
export const getEndorsement = (id: string): Promise<Response<any>> => axiosGet(`/endossement/${id}`);
export const patchEndorsement = (
  idEndo: string,
  text: string,
  status: string,
  typeEndo: string,
  idBadge:string,
): Promise<Response<any>> =>
  axiosPatch(`/endossement/${idEndo}`, {
    data: {
      idBadge,
      text,
      status,
      typeEndo,
    },
  });

export const addAgrement = (
  email: string,
  idBadge: string,
  name: string,
  typeEndo: 'badge' | 'benif' | 'emetteur',
): Promise<Response<any>> =>
  axiosPost('/endossement', {
    data: {
      email,
      idBadge,
      name,
      typeEndo,
    },
  });

export const getAllEndossementById = (id: string): Promise<Response<any>> => axiosGet(`/endossement/badge/${id}`);
export const getAllEndossementByUser = (id: string): Promise<Response<any>> => axiosGet(`/endossement/user/${id}`);
