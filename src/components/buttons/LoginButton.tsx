import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

export const LoginButton = withStyles({
  root: {
    boxShadow: 'none',
    textTransform: 'none',
    fontSize: 16,
    padding: '6px 12px',
    border: '1px solid',
    borderRadius: '4px',
    lineHeight: 1.5,
    backgroundColor: '#ed1e79',
    color: '#FFFFFF',
    width: 373,
    height: 58,
    borderColor: '#ed1e79',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#ed1e79',
      borderColor: '#ed1e79',
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#ed1e79',
      borderColor: '#ed1e79',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem #ed1e79',
    },
  },
})(Button);
