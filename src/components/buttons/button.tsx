import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button, { ButtonProps } from '@material-ui/core/Button';

import classNames from 'utils/classNames';

interface Props extends ButtonProps {
  children?: React.ReactChild;
  hasbadge?: boolean;
  badgeCount?: number;
  bcolor?: string;
  onClick?: () => void;
  iconName?: 'arrow' | 'person' | 'add' | 'loading' | 'done' | 'clear' | 'file' | 'upload';
  hasIcon?: boolean;
  startIcon?: boolean;
  endIcon?: boolean;
  className?: string;
  variant?: 'text' | 'outlined' | 'contained';
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      position: 'relative',
      borderRadius: 4,
      color: '#fff',
      fontWeight: 600,
      fontFamily: 'como',
      height: 45,
      '&:hover': {
        backgroundColor: '#ed1e79',
        borderColor: '#ed1e79',
        fontFamily: 'como',
      },
    },
    badge: {
      position: 'absolute',
      right: 10,
      background: '#fff',
      color: '#000',
      top: -15,
      borderRadius: 50,
      height: 25,
      width: 25,
      fontSize: 14,
      fontWeight: 400,
    },
  }));
export default function CButton({
  children,
  onClick,
  bcolor,
  iconName,
  className,
  variant,
  ...other
}: Props) {
  const classes = useStyles();

  return (
    <Button
      {...other}
      variant={variant}
      color="primary"
      className={classNames(classes.button, className)}
      onClick={onClick}
      style={{ backgroundColor: bcolor }}
    >
      {children}
    </Button>
  );
}