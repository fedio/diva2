import React from 'react';
import Button, { ButtonProps } from '@material-ui/core/Button/Button';
import classNames from 'utils/classNames';

import './scss/submitButton.scss';

const SubmitButton = ({ className, ...other }: ButtonProps) => (
  <Button
    {...other}
    className={classNames('components_buttons_submit_button', className)}
  />
);

export default SubmitButton;
