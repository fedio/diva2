import React, { useRef, useEffect, useState } from 'react';
import Transition from 'react-transition-group/Transition';
import './scss/dropdown.scss';

interface Props {
  open: boolean;
  children?: React.ReactChild;
}

const DropDown = ({ open, children }: Props) => {
  const content = useRef<HTMLDivElement>(null);
  const [ready, setReady] = useState(false);
  const [height, setHeight] = useState<string | number>(0);

  useEffect(() => {
    const div = content.current;

    setTimeout(() => {
      if (div) {
        const h = div.getBoundingClientRect().height;
        if (h === height) setReady(open);
        else setHeight(div.getBoundingClientRect().height);
      }
    }, 50);
  }, [open]);

  useEffect(() => {
    if (height !== 'none') {
      setTimeout(() => {
        setReady(open);
      }, 50);
    }
  }, [height]);

  useEffect(() => {
    if (ready) {
      setTimeout(() => {
        if (ready) {
          setHeight('none');
        }
      }, 400);
    }
  }, [ready]);

  return (
    <Transition timeout={300} in={ready}>
      {(state) => (
        <div className="components_ui_doropdown" style={{ maxHeight: state === 'entered' ? height : 0 }}>
          <div className="doropdown_content" ref={content}>
            {children}
          </div>
        </div>
      )}
    </Transition>
  );
};

export default DropDown;
