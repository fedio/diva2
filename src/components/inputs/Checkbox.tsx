import React from 'react';
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox/Checkbox';
import classNames from 'utils/classNames';

import Label from './Label';

import './scss/checkbox.scss';

interface Props extends CheckboxProps {
  label: string;
  children?: React.ReactChild;
}

const CheckBox = ({
 label, children, className, ...other
}: Props) => (
  /* eslint-disable-next-line */
  <div className={classNames('components_inputs_checkbox', className)}>
    <Checkbox {...other} />
    <Label className="checkbox_label">{label}</Label>
    {children}
  </div>
);

export default CheckBox;
