import React, { useState, useRef, useEffect } from 'react';
import Button from '@material-ui/core/Button/Button';
import ColorPicker from 'components/pickers/ColorPicker';
import CreateBadgeIconsCarousel from 'components/badge/CreateBadgeIconsCarousel';
import EditModelText from 'components/badge/EditModelText';
import EditModelIcon from 'components/badge/EditModelIcon';

import { useDidUpdate } from 'hooks/useLifeCycle';

import './scss/badgeModelInput.scss';
import { readAsText } from 'utils/fileReader';
import useApiState from 'hooks/useApiState';
import { ListIcons } from 'requests/model';

interface Color {
  color: string;
  id: string;
  nodeName: string;
}

interface Props {
  blob: Blob;
  onChangeModel: () => void;
  modelId: string;
}

const BadgeModelInput = ({ blob, onChangeModel, modelId }: Props) => {
  const [colors, setColors] = useState<Color[]>([]);
  const [pickerOpen, setPickerOpen] = useState(-1);
  const [selectedIcon, setSelectedIcon] = useState('');
  const [svgMounted, setSvgMounted] = useState(false);
  const [state, getListIcons] = useApiState(ListIcons);
  const emptyArrayRef = useRef([]);
  const prevBBoxRef = useRef<DOMRect | null>();
  const modelRef = useRef<HTMLDivElement>(null);

  function createSvg(svg: Node): Color[] {
    const result: { color: string; id: string; nodeName: string }[] = [];
    const { length } = svg.childNodes;
    let r: Color[] = [];

    for (let i = 0; i < length; i += 1) {
      const child = svg.childNodes[i];
      r = [...r, ...createSvg(child)];
      if (!child.nodeName.startsWith('#')) {
        const { nodeName } = child;
        const color = (child as Element).getAttribute('fill');

        if (color) {
          const found = result.find((node) => node.nodeName === nodeName && node.color === color);
          if (!found) {
            const id = `_${Math.random()
              .toString(36)
              .substr(2, 9)}`;
            result.push({
              id,
              color,
              nodeName,
            });
            (child as Element).id = id;
          } else {
            (child as Element).id = found.id;
          }
        }
      }
    }

    return [...result, ...r];
  }

  function handleSvg(text: string) {
    // eslint-disable-next-line
    (document.getElementById('badge-model-svg') as Element).querySelector('svg')?.remove();

    const parser = new DOMParser();
    const svg: Element = parser.parseFromString(text, 'image/svg+xml').childNodes[0] as any;
    const nextColors = createSvg(svg);
    setColors(nextColors);
    if (nextColors.length) {
      const path = svg.querySelector(`#${nextColors[0].id}`);
      if (path) {
        setSelectedIcon(path.getAttribute('d') || '');
      }
    }
    svg.setAttribute('height', '420px');
    svg.setAttribute('width', '420px');
    if (!modelRef.current) {
      (document.getElementById('badge-model-svg') as Element).appendChild(svg);
    } else {
      (document.getElementById('badge-model-svg') as Element).insertBefore(svg, modelRef.current);
    }

    setSvgMounted(true);
  }

  useEffect(() => {
    prevBBoxRef.current = null;
    readAsText(blob).then((text) => handleSvg(text));
    getListIcons(modelId);
    // eslint-disable-next-line
  }, [blob]);

  useDidUpdate(() => {
    colors.forEach((c) => {
      document.querySelectorAll(`#${c.id}`).forEach((el) => {
        const fill = el.getAttribute('fill');
        if (fill !== c.color) {
          el.setAttribute('fill', c.color);
        }
      });
    });
  }, [colors]);

  function onColorChange(c: string, i: number) {
    const newColors = [...colors];
    newColors[i] = { ...newColors[i], color: c };
    setColors(newColors);
  }

  function onIconChange(d: string) {
    if (colors.length) {
      const path = document.querySelector<SVGPathElement>(`#${colors[0].id}`);

      if (path) {
        const prevBox = prevBBoxRef.current || path.getBBox();
        if (!prevBBoxRef.current) {
          prevBBoxRef.current = prevBox;
        }
        path.setAttribute('d', d);
        // path.setAttribute('transform-origin', 'center');
        const newPath = document.querySelector<SVGPathElement>(`#${colors[0].id}`);
        if (newPath) {
          const newBox = newPath.getBBox();

          /*  const scaleValue = Math.min(prevBox.height / newBox.height, prevBox.width / newBox.width);

        const scale = `scale(${scaleValue})`; */

          const translate = `translate(${prevBox.x - newBox.x + (prevBox.width - newBox.width) / 2},${prevBox.y
            - newBox.y
            + (prevBox.height - newBox.height) / 2})`;
          newPath.setAttribute('transform', `${translate}`);
          setSelectedIcon(d);
        }
      }
    }
  }

  function renderColor({ color, id }: Color, index: number) {
    return (
      <div
        onClick={() => setPickerOpen(index)}
        key={id}
        className="badge_model_color"
        style={{ backgroundColor: color }}
      >
        <ColorPicker
          onClose={() => {
            setTimeout(() => {
              setPickerOpen((picker) => (picker === index ? -1 : picker));
            }, 300);
          }}
          onSelect={(c) => onColorChange(c, index)}
          open={pickerOpen === index}
        />
      </div>
    );
  }

  return (
    <div className="components_inputs_badge_model_input">
      <div className="badge_model_input_svg_container flex_center" id="badge-model-svg">
        {svgMounted && (
          <div ref={modelRef} className="width_100 flex_column padding_15">
            <EditModelText className="flex_1" id="badge-model-svg" />
            <EditModelIcon path={selectedIcon} className="flex_1 margin_v_20" id="badge-model-svg" />
          </div>
        )}
      </div>
      <div className="badge_model_input_change_container">
        <div className="badge_model_title">Modèle</div>
        <Button onClick={onChangeModel} className="badge_model_button">
          Choisir un modèle...
        </Button>
        <div className="badge_model_title badge_model_colors_title">Couleurs</div>
        <div className="badge_model_colors">{colors.map(renderColor)}</div>
        <div className="badge_model_title badge_model_icons_title">Pictogramme</div>
        <CreateBadgeIconsCarousel
          className="badge_model_colors"
          icons={state.data || emptyArrayRef.current}
          onIconChange={onIconChange}
          selectedIcon={selectedIcon}
        />
      </div>
    </div>
  );
};

export default BadgeModelInput;
