import React, { useRef, useState } from 'react';
import Button from '@material-ui/core/Button/Button';
import './scss/YNQInput.scss';
import classNames from 'utils/classNames';
import Label from './Label';

interface Props {
  label: string;
  checked?: boolean;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  className?: string;
  name?: string;
}

const YNQInput = ({
 label, checked, onChange: onChangeProp, className, name,
}: Props) => {
  const input = useRef<HTMLInputElement>(null);
  const [checkedState, setCheckedState] = useState(!!checked);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCheckedState(e.target.checked);
    if (onChangeProp) onChangeProp(e);
  };

  const onSelect = () => {
    const checkbox = input.current;
    if (checkbox) {
      checkbox.click();
    }
  };

  return (
    <div className={classNames('components_inputs_ynq_input', className)}>
      <Label>{label}</Label>
      <div className="ynq_input_choices">
        <Button disabled={checkedState} onClick={onSelect} className="ynq_input_choice">
          Oui
        </Button>
        <Button disabled={!checkedState} onClick={onSelect} className="ynq_input_choice">
          Non
        </Button>
      </div>
      <input name={name} ref={input} type="checkbox" checked={checkedState} onChange={onChange} />
    </div>
  );
};

export default YNQInput;
