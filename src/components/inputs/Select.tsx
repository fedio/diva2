import React from 'react';
import SelectBase, { SelectProps } from '@material-ui/core/Select/Select';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import ErrorText from 'components/inputs/ErrorText';
import Label from 'components/inputs/Label';

import classNames from 'utils/classNames';
import './scss/select.scss';

export interface Option {
  value: string;
  label: string;
}

interface Props extends Omit<SelectProps, 'variant'> {
  label: string;
  options: Option[];
  errorText?: string;
}
const Select = ({
 label, className, classes: c, options, errorText, ...rest
}: Props) => {
  const classes = c || {};

  return (
    <div
      className={classNames(
        'components_inputs_select',
        errorText !== undefined && 'components_inputs_input_with_error',
        className,
      )}
    >
      <Label>{label}</Label>
      <SelectBase
        placeholder="Sélectionner"
        {...rest}
        classes={{
          selectMenu: classNames('select_input_menu', classes.selectMenu),
          icon: classNames('select_input_icon', classes.icon),
          ...classes,
        }}
        className="select_input"
        variant="outlined"
        error={!!errorText}
      >
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </SelectBase>
      {errorText !== undefined && <ErrorText>{errorText}</ErrorText>}
    </div>
  );
};

export default Select;
