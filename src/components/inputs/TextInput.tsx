import React, { forwardRef, Ref } from 'react';
import TextField, { OutlinedTextFieldProps } from '@material-ui/core/TextField/TextField';
import Label from 'components/inputs/Label';
import ErrorText from 'components/inputs/ErrorText';

import classNames from 'utils/classNames';
import './scss/textInputs.scss';

interface Props extends Omit<OutlinedTextFieldProps, 'variant'> {
  label?: string;
  errorText?: string;
}

const TextInput = forwardRef(
  ({
 label, inputProps, className, errorText, children, ...rest
}: Props, ref: Ref<HTMLInputElement>) => {
    const inputPropsDefault = inputProps || {};
    return (
      <div
        className={classNames(
          'components_inputs_text_input',
          errorText !== undefined && 'components_inputs_input_with_error',
          className,
        )}
      >
        {label && <Label>{label}</Label>}
        <TextField
          error={!!errorText}
          className="text_input_input"
          inputProps={{ ...inputPropsDefault, ref }}
          autoComplete="off"
          {...rest}
          variant="outlined"
        />
        {errorText !== undefined && <ErrorText>{errorText}</ErrorText>}
      </div>
    );
  },
);

export default TextInput;
