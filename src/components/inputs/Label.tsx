import React from 'react';
import './scss/label.scss';
import classNames from 'utils/classNames';

const Label = ({
  children,
  className,
  ...rest
}: React.DetailedHTMLProps<React.LabelHTMLAttributes<HTMLDivElement>, HTMLDivElement>) => (
  // eslint-disable-next-line
  <div {...rest} className={classNames('components_inputs_label', className)}>
    {children}
  </div>
);

export default Label;
