import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import './scss/notifPanel.scss';
import classNames from 'utils/classNames';

interface Props extends RouteComponentProps {
  className?: string;
  data: any;
}

const NotifPanel = ({ className, data, history }: Props) => {
  const onClick = async (id: string, idNotif: string, detail: string) => {
    if (detail === 'demande') {
      history.push({ pathname: `/delivrances_details/${id}`, state: { id: idNotif } });
    } else if (detail === 'answer') {
      history.push({ pathname: '/profile/mybadges', state: { id: idNotif } });
    }
  };
  return (
    <div className={classNames('container_notif_panel', className)}>
      <div className="container_notif_panel_header">
        <span>Notifcations</span>
        <hr />
      </div>
      {data
        && data.map((el: any) => (
          <div
            className={el.isRead ? 'container_notif_panel_notif_body' : 'container_notif_panel_notif_body_blue'}
            key={el._id}
            onClick={() => onClick(el.idCandidature, el._id, el.detail)}
          >
            <span className="container_notif_panel_notif_body_user">{el.from}</span>
            <span className="containernotif_panel_notif_body_demande">{el.text}</span>
            <span className="containernotif_panel_notif_body_date">{el.date}</span>
            <hr />
          </div>
        ))}
    </div>
  );
};
export default withRouter(NotifPanel);
