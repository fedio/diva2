import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CheckCircle from '@material-ui/icons/CheckCircleOutline'
import MButton from '@material-ui/core/Button'

import Button from 'components/button';
import './scss/agrementModal.scss';

interface Props {
  open: boolean;
  handleClose: () => void;
  children?: React.ReactNode;
  handleSubmit: () => void;
}

const AgrementModal = ({
 handleClose, open, children, handleSubmit, ...other
}: Props) => (
  /* const [email, setEmail] = useState<string>('');
  const handleChange = (e: any) => {
    setEmail(e.target.value);
  }; */
  <div className="AgrementModalContainer">
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle className="modalTitle" id="alert-dialog-title">
      <div className="modal_header" >
        <CheckCircle style={{color: "#fc5e2e"}} />
       <span className="modal_header_title">Demande d&apos;endossement</span> 
        </div>  
      </DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions className="dialogActions">
        <MButton  variant="outlined" className="button_modal_fermer" onClick={handleClose}>
          Fermer
        </MButton>
        <Button className="button_modal_activation" onClick={handleSubmit}>
          envoyer
        </Button>
      </DialogActions>
    </Dialog>
  </div>
);
export default AgrementModal;
