import React, { forwardRef, Ref, useState } from 'react';

import useApiState from 'hooks/useApiState';
import { useDidMount } from 'hooks/useLifeCycle';
import { ListModel, Model } from 'requests/model';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';

import classNames from 'utils/classNames';
import SubmitButton from 'components/buttons/SubmitButton';

import './scss/modelSelect.scss';

interface Props {
  onClose?: () => void;
  onSelect: (id: Model | null) => void;
}

const ModelSelect = forwardRef(({ onClose, onSelect }: Props, ref: Ref<HTMLDivElement>) => {
  const [state, getListModel] = useApiState(ListModel);
  const [selectedModel, setSelectedModel] = useState<Model | null>(null);

  useDidMount(() => {
    getListModel();
  });

  return (
    <div ref={ref} className="width_100 height_100 flex_center" tabIndex={-1}>
      <div className="components_modals_model_select flex_column">
        <div className="model_select_title">Sélectionnez un modèle de badge</div>
        <div className={classNames('model_select_container flex_row', !state.data && 'flex_center')}>
          {state.data ? (
            state.data.map((model) => (
              <div
                className={classNames(
                  'model_select_model',
                  model._id === selectedModel?._id && 'model_select_selected_model',
                )}
                key={model._id}
              >
                <img
                  onClick={() => setSelectedModel(model)}
                  className="model_select_img"
                  src={model.file}
                  alt="model"
                />
              </div>
            ))
          ) : (
            <CircularProgress />
          )}
        </div>
        <div className="model_select_footer flex_row width_100">
          <div onClick={onClose} className="modal_select_close">
            Fermer
          </div>
          <SubmitButton onClick={() => onSelect(selectedModel)}>Sélectionner</SubmitButton>
        </div>
      </div>
    </div>
  );
});

export default ModelSelect;
