import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from 'components/button';
import './scss/modalActiveUser.scss';

interface Props {
  open: boolean;
  handleClose: () => void;
}

const RegisterModal = ({ handleClose, open, ...other }: Props) => (
  <div>
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle className="modalTitle" id="alert-dialog-title">
        Activation du Compte
        {' '}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Vous allez recevoir un mail de confirmation. Merci de vérifier vos emails.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button className="button_modal_activation" onClick={handleClose}>
          OK
        </Button>
      </DialogActions>
    </Dialog>
  </div>
);
export default RegisterModal;
