import React, { useEffect, useState } from 'react';
import { Carousel } from 'react-responsive-carousel';

import Icon from 'components/ui/Icon';
import classNames from 'utils/classNames';

import ArrowRight from 'assets/icons/arrow-right.svg';

import 'react-responsive-carousel/lib/styles/carousel.min.css';
import './scss/createBadgeIconsCarousel.scss';

interface Props {
  icons: string[];
  selectedIcon: string;
  onIconChange: (d: string) => void;
  className?: string;
}

const CreateBadgeIconsCarousel = ({
 icons, selectedIcon, onIconChange, className,
}: Props) => {
  const [slideIcons, setSlideIcons] = useState<string[][]>([]);
  const [selectedItem, setSelectedItem] = useState(0);

  useEffect(() => {
    const formatedIcons: string[][] = [];

    icons.forEach((icon) => {
      const last = formatedIcons[formatedIcons.length - 1];
      if (last && last.length < 6) {
        last.push(icon);
      } else {
        formatedIcons.push([icon]);
      }
    });

    setSlideIcons(formatedIcons);
  }, [icons]);

  function showPrevious() {
    setSelectedItem((selectedItem - 1) % slideIcons.length);
  }

  function showNext() {
    setSelectedItem((selectedItem + 1) % slideIcons.length);
  }

  if (slideIcons.length === 0) return null;

  return (
    <div className={classNames('components_badge_create_badge_icons_carousel', className)}>
      <Carousel
        selectedItem={selectedItem}
        axis="horizontal"
        showThumbs={false}
        showIndicators={false}
        showStatus={false}
        showArrows={false}
      >
        {slideIcons.map((iconsSlide, index) => (
          // eslint-disable-next-line
          <div className="create_badge_icons_container" key={index}>
            {iconsSlide.map((d) => (
              <Icon
                className="badge_model_icons"
                selected={d === selectedIcon}
                key={d}
                onClick={() => onIconChange(d)}
                d={d}
              />
            ))}
          </div>
        ))}
      </Carousel>
      <div className="create_badge_icons_arrow_container">
        <button disabled={selectedItem === 0} onClick={showPrevious} className="create_badge_icons_arrow flex_center">
          <img src={ArrowRight} alt="arrow-left" />
        </button>
        <button
          disabled={selectedItem === slideIcons.length - 1}
          onClick={showNext}
          className="create_badge_icons_arrow flex_center"
        >
          <img src={ArrowRight} alt="arrow-right" className="img_arrow_right" />
        </button>
      </div>
    </div>
  );
};

export default CreateBadgeIconsCarousel;
