import React, { memo, useRef } from 'react';
import CheckCircle from '@material-ui/icons/CheckCircleOutline';
import Edit from '@material-ui/icons/Edit';
import Close from '@material-ui/icons/Close';

import classNames from 'utils/classNames';
import { useDidUpdate } from 'hooks/useLifeCycle';

import './scss/createBadgeStep.scss';

interface Props {
  open: boolean;
  title: string;
  subTitle: string;
  children?: React.ReactChild;
  done?: boolean;
  className?: string;
  onOpen?: (open: boolean) => void;
}

const CreateBadgeStep = ({
 open, title, subTitle, children, className, done, onOpen,
}: Props) => {
  const containerRef = useRef<HTMLDivElement>(null);

  useDidUpdate(() => {
    if (containerRef.current && open) {
      const { top } = containerRef.current.getBoundingClientRect();

      window.scrollTo({ left: 0, top, behavior: 'smooth' });
    }
  }, [open]);

  function onClick(e: React.MouseEvent<SVGSVGElement>) {
    e.stopPropagation();
    if (onOpen) {
      onOpen(!open);
    }
  }

  return (
    <div ref={containerRef} className={classNames('components_badge_create_badge_step', className)}>
      <div className={classNames('create_badge_step_content', open && 'create_badge_step_content_open')}>
        <div
          className={classNames(
            'flex_center create_badge_step_icon_container',
            open && 'create_badge_step_icon_container_open',
          )}
        >
          <CheckCircle
            className={classNames(
              'create_badge_step_icon',
              open && 'create_badge_step_icon_open',
              done && 'create_badge_step_icon_done',
            )}
          />
        </div>
        <span className={classNames('create_badge_step_title', (open || done) && 'create_badge_step_selected')}>
          {title}
        </span>
        {done && (
          <div
            className={classNames(
              'flex_center create_badge_step_edit_container',
              open && 'create_badge_step_icon_container_open',
            )}
          >
            {open ? (
              <Close onClick={onClick} className="create_badge_step_edit" />
            ) : (
              <Edit onClick={onClick} className="create_badge_step_edit" />
            )}
          </div>
        )}
      </div>

      {open && (
        <div className="create_badge_step_children_container flex_column">
          <div className="create_badge_step_subtitle">{subTitle}</div>
          {children}
        </div>
      )}
    </div>
  );
};

export default memo(CreateBadgeStep);
