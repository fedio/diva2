import React, { useState, useEffect } from 'react';

import classNames from 'utils/classNames';
import Slider from 'components/ui/Slider';

import { getIcon, getElementTransform, setElementTransform } from 'utils/svg';

import './scss/editModelIcon.scss';
import Label from 'components/inputs/Label';

interface Props {
  className?: string;
  id: string;
  path?: string;
}

const EditModelIcon = ({ className, id, path }: Props) => {
  const [transformX, setTransformX] = useState(0);
  const [transformY, setTransformY] = useState(0);

  useEffect(() => {
    const icon = getIcon(id);

    if (icon) {
      const { x, y } = getElementTransform(icon);
      setTransformX(x);
      setTransformY(y);
    }
    // eslint-disable-next-line
  }, [path]);

  useEffect(() => {
    const icon = getIcon(id);
    if (icon) {
      const { x, y } = setElementTransform(icon, transformX, transformY);
      const transform = icon.getAttribute('transform') || '';
      icon.setAttribute('transform', `${transform.slice(0, transform.indexOf('translate'))} translate(${x}, ${y})`);
    }
    // eslint-disable-next-line
  }, [transformX, transformY]);

  function onTransformXChange(e: React.ChangeEvent<any>, value: number | number[]) {
    setTransformX(value as number);
  }
  function onTransformYChange(e: React.ChangeEvent<any>, value: number | number[]) {
    setTransformY(value as number);
  }

  return (
    <div className={classNames('components_badge_edit_model_icon', className)}>
      <Label className="padding_h_15">Spécifique du l&apos;icon</Label>
      <div className="flex_row">
        <div className="flex_1 flex_column padding_h_15">
          <Slider label="X :" value={transformX} onChange={onTransformXChange} />
          <Slider label="Y :" value={transformY} onChange={onTransformYChange} />
        </div>
      </div>
    </div>
  );
};

export default EditModelIcon;
