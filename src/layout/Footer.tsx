import React from 'react';
import './scss/footer.scss';

const Footer = () => (
  <div className="container_footer">
    <div className="container_footer_contents">
      <span className="container_footer_contents_left">
        <span>Mentions légales</span>
        <span>Politiques de confidentialités</span>
        <span>Retrait des données personnelles</span>
      </span>
      <span className="container_footer_contents_right">@2019 Badgeons les Territoires. Tous droits réservés.</span>
    </div>
  </div>
  );

export default Footer;
