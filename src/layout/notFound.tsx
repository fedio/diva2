import React from 'react';

const notFound = () => <h1>404 not found</h1>;

export default notFound;
