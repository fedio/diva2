import React from 'react';
import './scss/footerLogo.scss';
import LogoFrance from 'assets/footer/france.png';
import Region from 'assets/footer/region.png';
import Federation from 'assets/footer/federation.png';
import Fondation from 'assets/footer/fondation.png';

const FooterLogo = () => (
  <div className="container_footerLogo">
    <div className="container_footerLogo_contents">
      <div className="container_footerLogo_contents_left">
        <div className="contents_left_text">
          <span>Qui sommes nous ?</span>
          <span>Mentions légales</span>
          <span>Politiques de confidentialités</span>
          <span>Retrait des données personnelles</span>
          <span className="copy-diva">©2020 diva-badges.fr</span>
        </div>
      </div>
      <div className="container_footerLogo_contents_right">
        <div className="contents_right_text">
          <span>NOS PARTENAIRES</span>
          <div className="contents_right_img">
            <img alt="logo" src={LogoFrance} className="img1" />
            <img alt="logo" src={Region} className="img2" />
            <img alt="logo" src={Federation} className="img3" />
            <img alt="logo" src={Fondation} className="img4" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default FooterLogo;
