import React from 'react';
import './scss/header.scss';
import { useHistory } from 'react-router-dom';
import Logo from 'assets/header/groupe.png';
import Button from 'components/buttons/button';

const Header = () => {
  const history = useHistory();
  const headerClick = () => {
    history.push('/');
  };

  return (
    <div className="container_header">
      <div className="container_header_title" onClick={headerClick}>
        <img src={Logo} alt="logo" />
      </div>
    </div>
  );
};

export default Header;
