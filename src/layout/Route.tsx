import React, { useContext, useState, useCallback } from 'react';

import { Redirect, Route as BaseRoute, RouteProps } from 'react-router-dom';

import navigationContext from 'contexts/navigationContext';

import userContext from 'contexts/userContext';
import Header from './Header';
import SideBar from './SideBar';
import Footer from './Footer';

import classNames from '../utils/classNames';
import { encodeUri } from '../utils/url';

interface Props extends RouteProps {
  protected?: boolean;
  addFooter?: boolean;
  addSideBar?: boolean;
  headerProps?: any;
}

const Route = ({
  protected: protectedProp,
  addFooter,
  headerProps,
  addSideBar,
  path,
  ...props
}: Props) => {
  const { user } = useContext(userContext);

  const [lastContextPath, setLastContextPath] = useState(path);
  const [contextHeaderProps, setContextHeaderProps] = useState(null);

  const contextChange = useCallback(
    (newContext: any) => {
      setContextHeaderProps(newContext);
      setLastContextPath(path);
    },
    [path],
  );

  if (!user && protectedProp) {
    return <Redirect to={`/login${encodeUri({ from: window.location.pathname + window.location.search })}`} />;
  }

  const component = (
    <div className={classNames('page', (addFooter || headerProps || addSideBar) && 'with_layout_page')}>
      <BaseRoute path={path} {...props} />
    </div>
  );

  return (
    <navigationContext.Provider value={{ setHeaderProps: contextChange }}>
      {headerProps && (
        <Header
          context={lastContextPath === path ? contextHeaderProps : null}
          {...headerProps}
          protected={protectedProp}
        />
      )}
      {addSideBar && <SideBar />}
      {component}
      {addFooter && <Footer />}
    </navigationContext.Provider>
  );
};

export default Route;
